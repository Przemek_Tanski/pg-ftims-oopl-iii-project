/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fizyka;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author Użytkownik
 */
public class GravitiWindow extends Window {

    /**
     * Creates new form OknoGrawitacja
     */
    public GravitiWindow() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        equationPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        mass1Field = new fizyka.FieldNumber();
        jLabel2 = new javax.swing.JLabel();
        mass2Field = new fizyka.FieldNumber();
        jLabel3 = new javax.swing.JLabel();
        distanceField = new fizyka.FieldNumber();
        countButton = new javax.swing.JButton();
        resultField = new fizyka.FieldNumber();
        descriptionPanel = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        opisTextArea = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new javax.swing.OverlayLayout(getContentPane()));

        equationPanel.setLayout(new java.awt.GridLayout(0, 1));

        jLabel1.setText("Mass  1 [kg]");
        equationPanel.add(jLabel1);

        mass1Field.setDomyslnyTekst("Enter mass");
        equationPanel.add(mass1Field);

        jLabel2.setText("Mass  2 [kg]");
        equationPanel.add(jLabel2);

        mass2Field.setText("Enter mass");
        mass2Field.setDomyslnyTekst("Enter mass");
        equationPanel.add(mass2Field);

        jLabel3.setText("Distance [m]");
        equationPanel.add(jLabel3);

        distanceField.setText("Enter distance");
        distanceField.setDomyslnyTekst("Enter distance");
        distanceField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                distanceFieldActionPerformed(evt);
            }
        });
        equationPanel.add(distanceField);

        countButton.setText("Calculate Force Gravityi [N]");
        countButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                countButtonActionPerformed(evt);
            }
        });
        equationPanel.add(countButton);

        resultField.setDomyslnyTekst("-");
        resultField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resultFieldActionPerformed(evt);
            }
        });
        equationPanel.add(resultField);

        jTabbedPane1.addTab("Equation", equationPanel);

        descriptionPanel.setLayout(new java.awt.BorderLayout());

        jLabel4.setText("Graviti");
        descriptionPanel.add(jLabel4, java.awt.BorderLayout.PAGE_START);

        opisTextArea.setEditable(false);
        opisTextArea.setColumns(20);
        opisTextArea.setLineWrap(true);
        opisTextArea.setRows(5);
        opisTextArea.setText("Graviti is a forced between two mass..");
        jScrollPane1.setViewportView(opisTextArea);

        descriptionPanel.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jTabbedPane1.addTab("Description", descriptionPanel);

        getContentPane().add(jTabbedPane1);
        jTabbedPane1.getAccessibleContext().setAccessibleName("tab2");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void countButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_countButtonActionPerformed
        
        
        try{
            
            double masa1 = mass1Field.getLiczbe();
            double masa2 = mass2Field.getLiczbe();
            double odleglosc = distanceField.getLiczbe();
            double G = 6.67408313131*0.00000000001;
            double wynik = (masa1*masa2)/(odleglosc*odleglosc)*G;
            resultField.setLiczbe(wynik);
        }catch(NumberFormatException e){
            resultField.setText("błąd");
        }
    }//GEN-LAST:event_countButtonActionPerformed

    private void resultFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resultFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_resultFieldActionPerformed

    private void distanceFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_distanceFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_distanceFieldActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GravitiWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GravitiWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GravitiWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GravitiWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(GravitiWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                    Logger.getLogger(GravitiWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(GravitiWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (UnsupportedLookAndFeelException ex) {
                    Logger.getLogger(GravitiWindow.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                new GravitiWindow().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton countButton;
    private javax.swing.JPanel descriptionPanel;
    private fizyka.FieldNumber distanceField;
    private javax.swing.JPanel equationPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private fizyka.FieldNumber mass1Field;
    private fizyka.FieldNumber mass2Field;
    private javax.swing.JTextArea opisTextArea;
    private fizyka.FieldNumber resultField;
    // End of variables declaration//GEN-END:variables
}
