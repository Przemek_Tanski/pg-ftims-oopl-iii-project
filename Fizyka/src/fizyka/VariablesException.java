/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fizyka;

/**
 *
 * @author Użytkownik
 */
public class VariablesException extends RuntimeException{  // My Exception
    private final String variableName;

    public VariablesException(String variableName) {
        this.variableName = variableName;
    }

    @Override
    public String getMessage() {
        return "Lack of variable " + variableName;
    }
}
