/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fizyka;

import static fizyka.SimpleRoadEquation.V_NAME;
import java.util.Map;

/**
 *
 * @author Użytkownik
 */
public class RoadEquation extends SimpleRoadEquation{

    public RoadEquation() {
        addVariable(A_NAME);
        getChartPainters().clear();
        addChartPainter(createRoadChartPrinter());
        addChartPainter(createVChartPrinter());
        
        setEvaluator(new Evaluator() {
            @Override
            public double calc(Map<String, Double> parametry) { // use map - parametr
                
                double v0 = parametry.get(V_NAME);
                double t = parametry.get(TIME_NAME);
                double a = parametry.get(A_NAME);
                
                return v0*t+a*t*t/2;
            }
        });
    }
    private static final String A_NAME = "a [m/s^2";

    private ChartPainter createRoadChartPrinter() {
        return new ChartPainter() {
            @Override
            public void paint(Chart.ChartGraphics cg, Map<String, Double> parametry) { // use map chart
            
                double t0 = 0;
                double v0 = parametry.get(V_NAME);
                double t1 = parametry.get(TIME_NAME);
                double a = parametry.get(A_NAME);
                
                double s0 = 0f;
                double s1 = v0*t1+a*t1*t1/2;
                cg.setRange(t0, t1,s0,s1);
                cg.drawX();
                cg.drawY();
                
                int steps = 10;
                double dt = (t1-t0)/steps;
                for(int i =0; i<steps;i++){
                    double T0 = t0+i*dt;
                    double T1 = t0+(i+1)*dt;
                    double S0 = v0*T0+a*T0*T0/2;
                    double S1 = v0*T1+a*T1*T1/2;
                    cg.drawLine(T0,S0,T1,S1);
                }
            }

            @Override
            public String getName() {
                return "s [m]";
            }
        };
    }

    private ChartPainter createVChartPrinter() {
        return new ChartPainter() {
            @Override
            public void paint(Chart.ChartGraphics cg, Map<String, Double> parametry) {
            
                
            
                double t0 = 0;
                double v0 = parametry.get(V_NAME);
                double t1 = parametry.get(TIME_NAME);
                double a = parametry.get(A_NAME);
                double v1 = v0+a*(t1-t0);
                cg.setRange(t0, t1,v0,v1);
                cg.drawX();
                cg.drawY();
                cg.drawLine(t0, v0, t1, v1);
                
            }

            @Override
            public String getName() {
                return "v [m/s]";
            }
        };
    }
    
    
}
