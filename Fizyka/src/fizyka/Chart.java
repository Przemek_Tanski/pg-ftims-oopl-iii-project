/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fizyka;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Map;
import javax.swing.JComponent;

/**
 *
 * @author Użytkownik
 */
public class Chart extends JComponent{

    public static final int STEPS = 10;
    public static final int MARGIN = 20;
    
    private ChartGraphics chartGraphics = new ChartGraphics();
    private Equation.ChartPainter painter;
    private Map<String, Double> params;
    
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        
        if (painter != null){
            chartGraphics.setGd(g2d);
            try{
                painter.paint(chartGraphics,params);
            }catch(Exception ex){
            }
        }
    }

    public void setParams(Map<String, Double> params) {
        this.params = params;
    }
    
    public void setPainter(Equation.ChartPainter painter) {
        this.painter = painter;
    }
    
    class ChartGraphics{
        private Graphics2D gd;
        private int endY;
        private int endX;
        
        private double x0;
        private double x1;
        private double y0;
        private double y1;

        public void setGd(Graphics2D gd) {
            this.gd = gd;
            endX = getWidth();
            endY = getHeight();
        }

        
        
        public Graphics2D getGd() {
            return gd;
        }
    
        public void drawX(){
            gd.drawLine(MARGIN, endY-MARGIN, endX-MARGIN, endY-MARGIN);
            
            double dt = (x1-x0)/STEPS;
            double dx = (endX-20)/12;

            for(int i = 0; i <= 11 ;i++){
                gd.drawString(String.format("%.2g%n",x0+dt*i),(int) (MARGIN+i*dx),(int) (endY-5f));
            }
        }
        
        public void drawY(){
            gd.drawLine(MARGIN, endY-MARGIN, MARGIN, MARGIN);
            
            double dy = (endY-20)/12;
            double dv = (y1-y0)/STEPS;

            for(int i = 0; i <= 11 ;i++){
                gd.drawString(String.format("%.2g%n",y0+dv*i),5,(int) (endY-MARGIN-i*dy));
            }
        }

        public void setRange(double x0, double x1, double y0, double y1) {
            this.x0 = x0;
            this.x1 = x1;
            this.y0 = y0;
            this.y1 = y1;
        }

        void drawLine(double X0, double Y0, double X1, double Y1) {
            // draw a chart
            
            int x0Position = (int) ((X0-x0)/(x1-x0)*(endX-2*MARGIN)*10/12);
            int y0Position = (int) ((Y0-y0)/(y1-y0)*(endY-2*MARGIN)*10/12);
            int x1Position = (int) ((X1-x0)/(x1-x0)*(endX-2*MARGIN)*10/12);
            int y1Position = (int) ((Y1-y0)/(y1-y0)*(endY-2*MARGIN)*10/12);
            
            gd.drawLine(x0Position+MARGIN, endY-MARGIN-y0Position, x1Position+MARGIN, endY-MARGIN-y1Position);
            
        }
        
        
        
    }
    
    
}
