/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fizyka;

import java.util.Map;

/**
 *
 * @author Użytkownik
 */
public class SimpleRoadEquation extends Equation{
    
    protected static final String TIME_NAME = "time [s]";
    protected static final String V_NAME = "v [s/t]";
    
    public SimpleRoadEquation() {
        super(" Count s! ", "s[t]", "s [m]", new RoadEvaluator(), V_NAME, TIME_NAME);
        this.addChartPainter(new RoadChartPrinter());
    }
    
    static class RoadEvaluator implements Evaluator{

        @Override
        public double calc(Map<String, Double> parametry) {
            double v= parametry.get(V_NAME);
            double t= parametry.get(TIME_NAME);
            return v*t;
        }
        
    }
    
    static class RoadChartPrinter implements ChartPainter{

        @Override
        public void paint(Chart.ChartGraphics gd, Map<String, Double> parametry) {
            
            double v= parametry.get(V_NAME);
            double t= parametry.get(TIME_NAME);
            final double s = t*v;
            
            gd.setRange(0, t, 0, s);
            gd.drawX();
            gd.drawY();
            gd.drawLine(0, 0, t, s);
            
        }

        @Override
        public String getName() {
            return "s [m]";
        }
    }

}
