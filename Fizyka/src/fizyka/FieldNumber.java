/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fizyka;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JTextField;

/**
 *
 * @author Użytkownik
 */
public class FieldNumber extends JTextField{
    
    private String domyslnyTekst;

    public FieldNumber() {
        
        addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                if (getText().equals(domyslnyTekst)){
                    setText("");
                }
                setForeground(Color.BLACK);
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (getText().equals(""))
                    setText(domyslnyTekst);
            }
        });
    }
    
    public void setDomyslnyTekst(String tekst){
        setText(tekst);
        domyslnyTekst = tekst;
    }
    
    public double getLiczbe(){
        try{
            return Double.parseDouble(getText()); // exception
        }catch(NumberFormatException exception){
            setForeground(Color.RED);
            throw exception;
        }
    }

    public void setLiczbe(double liczba) {
        setText(Double.toString(liczba));
    }
}
