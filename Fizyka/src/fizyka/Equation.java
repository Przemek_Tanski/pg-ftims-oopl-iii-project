/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fizyka;

import fizyka.Chart.ChartGraphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Użytkownik
 */
public class Equation {

    private final String opis;
    private final String nazwa;
    private final String doPoliczenia;
    private final List<Variable> variables = new ArrayList<>(); //array list 
    private Evaluator evaluator;
    private List<ChartPainter> chartPainters = new ArrayList<>(); // array list 
    
    public Equation(String opis, String nazwa, String doPoliczenia,Evaluator obliczacz,String... parametry) {
        this.opis = opis;
        this.nazwa = nazwa;
        this.doPoliczenia = doPoliczenia;
        this.evaluator = obliczacz;
        for(String nazwaParametru : parametry){
            variables.add(new Variable(nazwaParametru));
        }
    }

    protected Evaluator getEvaluator() {
        return evaluator;
    }
    
    public void setEvaluator(Evaluator evaluator) {
        this.evaluator = evaluator;
    }
        
    public void addVariable(String variableName){
        variables.add(new Variable(variableName));
    }

    public void addChartPainter(ChartPainter chartPainter) {
        chartPainters.add(chartPainter);
    }

    public List<ChartPainter> getChartPainters() {
        return chartPainters;
    }
    
    double licz(Map<String,Double> params){
        for(Variable var : variables){
            if (!params.keySet().contains(var.name))
                throw new VariablesException(var.name);
        }
        return evaluator.calc(params);
    }
    
    public List<Variable> getVariables() {
        return variables;
    }
    
    public String getOpis() {
        return opis;
    }

    public String getNazwa() {
        return nazwa;
    }

    public String getDoPoliczenia() {
        return doPoliczenia;
    }
    
    static class Variable{
        private String name;

        public Variable(String nazwa) {
            this.name = nazwa;
        }
        
        public String getName() {
            return name;
        }
    }
    
    
    public interface Evaluator{       // interface 
        double calc(Map<String,Double> parametry);
    }
    
    public interface ChartPainter{    // interface 
        void paint(ChartGraphics gd,Map<String,Double> parametry);
        public String getName();
    }
}
